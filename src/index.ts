import './css/style.scss'
import * as d3 from 'd3'

function helloElement(name='webpack') {
  const element = document.createElement('h1')
  element.innerHTML = d3.merge([['Hello'], [name]]).join(' ')

  return element
}

document.body.appendChild(helloElement('webpack'))
